
//----------------------------------------------------
// The following code was generated by CUP v0.11a beta 20060608
// Thu Jun 22 12:18:57 MSK 2017
//----------------------------------------------------

package verificator;

import java_cup.runtime.*;
import java_cup.runtime.Symbol;

/** CUP v0.11a beta 20060608 generated parser.
  * @version Thu Jun 22 12:18:57 MSK 2017
  */
public class parser extends java_cup.runtime.lr_parser {

  /** Default constructor. */
  public parser() {super();}

  /** Constructor which sets the default scanner. */
  public parser(java_cup.runtime.Scanner s) {super(s);}

  /** Constructor which sets the default scanner. */
  public parser(java_cup.runtime.Scanner s, java_cup.runtime.SymbolFactory sf) {super(s,sf);}

  /** Production table. */
  protected static final short _production_table[][] = 
    unpackFromStrings(new String[] {
    "\000\032\000\002\002\004\000\002\002\004\000\002\002" +
    "\004\000\002\002\002\000\002\003\006\000\002\005\003" +
    "\000\002\005\004\000\002\013\003\000\002\014\004\000" +
    "\002\014\005\000\002\007\003\000\002\007\003\000\002" +
    "\010\004\000\002\010\004\000\002\011\003\000\002\011" +
    "\004\000\002\011\005\000\002\011\006\000\002\011\006" +
    "\000\002\015\003\000\002\015\003\000\002\015\003\000" +
    "\002\004\010\000\002\006\003\000\002\012\004\000\002" +
    "\012\005" });

  /** Access to production table. */
  public short[][] production_table() {return _production_table;}

  /** Parse-action table. */
  protected static final short[][] _action_table = 
    unpackFromStrings(new String[] {
    "\000\051\000\010\002\ufffe\007\006\015\010\001\002\000" +
    "\004\002\053\001\002\000\010\002\ufffe\007\006\015\010" +
    "\001\002\000\004\006\041\001\002\000\010\002\ufffe\007" +
    "\006\015\010\001\002\000\004\013\011\001\002\000\012" +
    "\004\022\006\023\015\016\016\014\001\002\000\014\005" +
    "\ufff6\010\ufff6\011\ufff6\012\ufff6\014\ufff6\001\002\000\006" +
    "\011\ufffa\012\ufffa\001\002\000\030\004\uffed\005\uffed\006" +
    "\uffed\010\uffed\011\uffed\012\uffed\014\uffed\015\uffed\016\uffed" +
    "\017\uffed\020\uffed\001\002\000\006\011\ufffc\012\034\001" +
    "\002\000\030\004\uffee\005\uffee\006\uffee\010\uffee\011\uffee" +
    "\012\uffee\014\uffee\015\uffee\016\uffee\017\uffee\020\uffee\001" +
    "\002\000\030\004\022\005\ufff3\006\023\010\ufff3\011\ufff3" +
    "\012\ufff3\014\ufff3\015\016\016\014\017\ufff3\020\ufff3\001" +
    "\002\000\020\005\ufff7\010\ufff7\011\ufff7\012\ufff7\014\ufff7" +
    "\017\031\020\032\001\002\000\004\011\030\001\002\000" +
    "\012\004\022\006\023\015\016\016\014\001\002\000\030" +
    "\004\uffec\005\uffec\006\uffec\010\uffec\011\uffec\012\uffec\014" +
    "\uffec\015\uffec\016\uffec\017\uffec\020\uffec\001\002\000\004" +
    "\005\025\001\002\000\020\005\ufff1\010\ufff1\011\ufff1\012" +
    "\ufff1\014\ufff1\017\ufff1\020\ufff1\001\002\000\020\005\ufff0" +
    "\010\ufff0\011\ufff0\012\ufff0\014\ufff0\017\ufff0\020\ufff0\001" +
    "\002\000\020\005\uffef\010\uffef\011\uffef\012\uffef\014\uffef" +
    "\017\uffef\020\uffef\001\002\000\010\002\ufffd\007\ufffd\015" +
    "\ufffd\001\002\000\014\005\ufff5\010\ufff5\011\ufff5\012\ufff5" +
    "\014\ufff5\001\002\000\014\005\ufff4\010\ufff4\011\ufff4\012" +
    "\ufff4\014\ufff4\001\002\000\020\005\ufff2\010\ufff2\011\ufff2" +
    "\012\ufff2\014\ufff2\017\ufff2\020\ufff2\001\002\000\012\004" +
    "\022\006\023\015\016\016\014\001\002\000\004\011\ufffb" +
    "\001\002\000\006\011\ufff9\012\034\001\002\000\004\011" +
    "\ufff8\001\002\000\004\002\000\001\002\000\012\004\022" +
    "\006\023\015\016\016\014\001\002\000\004\010\uffea\001" +
    "\002\000\004\010\044\001\002\000\004\014\046\001\002" +
    "\000\004\011\051\001\002\000\012\004\022\006\023\015" +
    "\016\016\014\001\002\000\006\011\uffe9\014\046\001\002" +
    "\000\004\011\uffe8\001\002\000\010\002\uffeb\007\uffeb\015" +
    "\uffeb\001\002\000\004\002\uffff\001\002\000\004\002\001" +
    "\001\002" });

  /** Access to parse-action table. */
  public short[][] action_table() {return _action_table;}

  /** <code>reduce_goto</code> table. */
  protected static final short[][] _reduce_table = 
    unpackFromStrings(new String[] {
    "\000\051\000\010\002\003\003\006\004\004\001\001\000" +
    "\002\001\001\000\010\002\051\003\006\004\004\001\001" +
    "\000\002\001\001\000\010\002\037\003\006\004\004\001" +
    "\001\000\002\001\001\000\016\005\020\007\012\010\011" +
    "\011\017\013\014\015\016\001\001\000\002\001\001\000" +
    "\002\001\001\000\002\001\001\000\004\014\034\001\001" +
    "\000\002\001\001\000\006\011\032\015\016\001\001\000" +
    "\002\001\001\000\002\001\001\000\012\007\023\010\011" +
    "\011\017\015\016\001\001\000\002\001\001\000\002\001" +
    "\001\000\002\001\001\000\002\001\001\000\002\001\001" +
    "\000\002\001\001\000\002\001\001\000\002\001\001\000" +
    "\002\001\001\000\012\007\035\010\011\011\017\015\016" +
    "\001\001\000\002\001\001\000\004\014\036\001\001\000" +
    "\002\001\001\000\002\001\001\000\014\006\042\007\041" +
    "\010\011\011\017\015\016\001\001\000\002\001\001\000" +
    "\002\001\001\000\004\012\044\001\001\000\002\001\001" +
    "\000\012\007\046\010\011\011\017\015\016\001\001\000" +
    "\004\012\047\001\001\000\002\001\001\000\002\001\001" +
    "\000\002\001\001\000\002\001\001" });

  /** Access to <code>reduce_goto</code> table. */
  public short[][] reduce_table() {return _reduce_table;}

  /** Instance of action encapsulation class. */
  protected CUP$parser$actions action_obj;

  /** Action encapsulation object initializer. */
  protected void init_actions()
    {
      action_obj = new CUP$parser$actions(this);
    }

  /** Invoke a user supplied parse action. */
  public java_cup.runtime.Symbol do_action(
    int                        act_num,
    java_cup.runtime.lr_parser parser,
    java.util.Stack            stack,
    int                        top)
    throws java.lang.Exception
  {
    /* call code in generated class */
    return action_obj.CUP$parser$do_action(act_num, parser, stack, top);
  }

  /** Indicates start state. */
  public int start_state() {return 0;}
  /** Indicates start production. */
  public int start_production() {return 0;}

  /** <code>EOF</code> Symbol index. */
  public int EOF_sym() {return 0;}

  /** <code>error</code> Symbol index. */
  public int error_sym() {return 1;}



public void report_error(String message, Object info) {
    StringBuilder m = new StringBuilder("Error ");

    if (info instanceof java_cup.runtime.Symbol)
      m.append( "("+info.toString()+")" );

    m.append(" : "+message);

    System.out.println(m);
  }

  public void report_fatal_error(String message, Object info) {
    report_error(message, info);
    throw new RuntimeException("Fatal Syntax Error");
  }

}

/** Cup generated class to encapsulate user supplied action code.*/
class CUP$parser$actions {
  private final parser parser;

  /** Constructor */
  CUP$parser$actions(parser parser) {
    this.parser = parser;
  }

  /** Method with the actual generated action code. */
  public final java_cup.runtime.Symbol CUP$parser$do_action(
    int                        CUP$parser$act_num,
    java_cup.runtime.lr_parser CUP$parser$parser,
    java.util.Stack            CUP$parser$stack,
    int                        CUP$parser$top)
    throws java.lang.Exception
    {
      /* Symbol object for return from actions */
      java_cup.runtime.Symbol CUP$parser$result;

      /* select the action based on the action number */
      switch (CUP$parser$act_num)
        {
          /*. . . . . . . . . . . . . . . . . . . .*/
          case 25: // res ::= EQUAL simple_type res 
            {
              Node RESULT =null;

              CUP$parser$result = parser.getSymbolFactory().newSymbol("res",8, ((java_cup.runtime.Symbol)CUP$parser$stack.elementAt(CUP$parser$top-2)), ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), RESULT);
            }
          return CUP$parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 24: // res ::= EQUAL simple_type 
            {
              Node RESULT =null;

              CUP$parser$result = parser.getSymbolFactory().newSymbol("res",8, ((java_cup.runtime.Symbol)CUP$parser$stack.elementAt(CUP$parser$top-1)), ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), RESULT);
            }
          return CUP$parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 23: // args_type ::= simple_type 
            {
              Node RESULT =null;

              CUP$parser$result = parser.getSymbolFactory().newSymbol("args_type",4, ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), RESULT);
            }
          return CUP$parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 22: // func ::= LCHEVRON NAME args_type RCHEVRON res SEMICOLON 
            {
              Node RESULT =null;
		RESULT = new Node("FUNC", -1);
              CUP$parser$result = parser.getSymbolFactory().newSymbol("func",2, ((java_cup.runtime.Symbol)CUP$parser$stack.elementAt(CUP$parser$top-5)), ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), RESULT);
            }
          return CUP$parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 21: // end_type ::= NAME 
            {
              Node RESULT =null;
		int stleft = ((java_cup.runtime.Symbol)CUP$parser$stack.peek()).left;
		int stright = ((java_cup.runtime.Symbol)CUP$parser$stack.peek()).right;
		LexerToken st = (LexerToken)((java_cup.runtime.Symbol) CUP$parser$stack.peek()).value;
		RESULT = new Node(st.name, st.start);
              CUP$parser$result = parser.getSymbolFactory().newSymbol("end_type",11, ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), RESULT);
            }
          return CUP$parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 20: // end_type ::= METAVARIABLE 
            {
              Node RESULT =null;
		int stleft = ((java_cup.runtime.Symbol)CUP$parser$stack.peek()).left;
		int stright = ((java_cup.runtime.Symbol)CUP$parser$stack.peek()).right;
		LexerToken st = (LexerToken)((java_cup.runtime.Symbol) CUP$parser$stack.peek()).value;
		RESULT = new Node(st.name, st.start);
              CUP$parser$result = parser.getSymbolFactory().newSymbol("end_type",11, ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), RESULT);
            }
          return CUP$parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 19: // end_type ::= VARIABLE 
            {
              Node RESULT =null;
		int stleft = ((java_cup.runtime.Symbol)CUP$parser$stack.peek()).left;
		int stright = ((java_cup.runtime.Symbol)CUP$parser$stack.peek()).right;
		LexerToken st = (LexerToken)((java_cup.runtime.Symbol) CUP$parser$stack.peek()).value;
		RESULT = new Node(st.name, st.start);
              CUP$parser$result = parser.getSymbolFactory().newSymbol("end_type",11, ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), RESULT);
            }
          return CUP$parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 18: // term_type ::= LPAREN simple_type RPAREN ATLEAST 
            {
              Node RESULT =null;
		RESULT = new Node("PARENS", -1);
              CUP$parser$result = parser.getSymbolFactory().newSymbol("term_type",7, ((java_cup.runtime.Symbol)CUP$parser$stack.elementAt(CUP$parser$top-3)), ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), RESULT);
            }
          return CUP$parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 17: // term_type ::= LPAREN simple_type RPAREN MANY 
            {
              Node RESULT =null;
		RESULT = new Node("PARENS", -1);
              CUP$parser$result = parser.getSymbolFactory().newSymbol("term_type",7, ((java_cup.runtime.Symbol)CUP$parser$stack.elementAt(CUP$parser$top-3)), ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), RESULT);
            }
          return CUP$parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 16: // term_type ::= LPAREN simple_type RPAREN 
            {
              Node RESULT =null;
		RESULT = new Node("PARENS", -1);
              CUP$parser$result = parser.getSymbolFactory().newSymbol("term_type",7, ((java_cup.runtime.Symbol)CUP$parser$stack.elementAt(CUP$parser$top-2)), ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), RESULT);
            }
          return CUP$parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 15: // term_type ::= end_type term_type 
            {
              Node RESULT =null;

              CUP$parser$result = parser.getSymbolFactory().newSymbol("term_type",7, ((java_cup.runtime.Symbol)CUP$parser$stack.elementAt(CUP$parser$top-1)), ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), RESULT);
            }
          return CUP$parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 14: // term_type ::= end_type 
            {
              Node RESULT =null;

              CUP$parser$result = parser.getSymbolFactory().newSymbol("term_type",7, ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), RESULT);
            }
          return CUP$parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 13: // stretch_type ::= term_type ATLEAST 
            {
              Node RESULT =null;

              CUP$parser$result = parser.getSymbolFactory().newSymbol("stretch_type",6, ((java_cup.runtime.Symbol)CUP$parser$stack.elementAt(CUP$parser$top-1)), ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), RESULT);
            }
          return CUP$parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 12: // stretch_type ::= term_type MANY 
            {
              Node RESULT =null;

              CUP$parser$result = parser.getSymbolFactory().newSymbol("stretch_type",6, ((java_cup.runtime.Symbol)CUP$parser$stack.elementAt(CUP$parser$top-1)), ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), RESULT);
            }
          return CUP$parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 11: // simple_type ::= stretch_type 
            {
              Node RESULT =null;

              CUP$parser$result = parser.getSymbolFactory().newSymbol("simple_type",5, ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), RESULT);
            }
          return CUP$parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 10: // simple_type ::= term_type 
            {
              Node RESULT =null;

              CUP$parser$result = parser.getSymbolFactory().newSymbol("simple_type",5, ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), RESULT);
            }
          return CUP$parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 9: // n_simple_type ::= OR simple_type n_simple_type 
            {
              Node RESULT =null;

              CUP$parser$result = parser.getSymbolFactory().newSymbol("n_simple_type",10, ((java_cup.runtime.Symbol)CUP$parser$stack.elementAt(CUP$parser$top-2)), ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), RESULT);
            }
          return CUP$parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 8: // n_simple_type ::= OR simple_type 
            {
              Node RESULT =null;

              CUP$parser$result = parser.getSymbolFactory().newSymbol("n_simple_type",10, ((java_cup.runtime.Symbol)CUP$parser$stack.elementAt(CUP$parser$top-1)), ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), RESULT);
            }
          return CUP$parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 7: // f_simple_type ::= simple_type 
            {
              Node RESULT =null;

              CUP$parser$result = parser.getSymbolFactory().newSymbol("f_simple_type",9, ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), RESULT);
            }
          return CUP$parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 6: // simple_types ::= f_simple_type n_simple_type 
            {
              Node RESULT =null;

              CUP$parser$result = parser.getSymbolFactory().newSymbol("simple_types",3, ((java_cup.runtime.Symbol)CUP$parser$stack.elementAt(CUP$parser$top-1)), ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), RESULT);
            }
          return CUP$parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 5: // simple_types ::= f_simple_type 
            {
              Node RESULT =null;

              CUP$parser$result = parser.getSymbolFactory().newSymbol("simple_types",3, ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), RESULT);
            }
          return CUP$parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 4: // type_def ::= VARIABLE VAREQUAL simple_types SEMICOLON 
            {
              Node RESULT =null;
		int stleft = ((java_cup.runtime.Symbol)CUP$parser$stack.elementAt(CUP$parser$top-3)).left;
		int stright = ((java_cup.runtime.Symbol)CUP$parser$stack.elementAt(CUP$parser$top-3)).right;
		LexerToken st = (LexerToken)((java_cup.runtime.Symbol) CUP$parser$stack.elementAt(CUP$parser$top-3)).value;
		RESULT = new Node(st.name, st.start);
              CUP$parser$result = parser.getSymbolFactory().newSymbol("type_def",1, ((java_cup.runtime.Symbol)CUP$parser$stack.elementAt(CUP$parser$top-3)), ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), RESULT);
            }
          return CUP$parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 3: // spec ::= 
            {
              Node RESULT =null;

              CUP$parser$result = parser.getSymbolFactory().newSymbol("spec",0, ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), RESULT);
            }
          return CUP$parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 2: // spec ::= func spec 
            {
              Node RESULT =null;

              CUP$parser$result = parser.getSymbolFactory().newSymbol("spec",0, ((java_cup.runtime.Symbol)CUP$parser$stack.elementAt(CUP$parser$top-1)), ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), RESULT);
            }
          return CUP$parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 1: // spec ::= type_def spec 
            {
              Node RESULT =null;

              CUP$parser$result = parser.getSymbolFactory().newSymbol("spec",0, ((java_cup.runtime.Symbol)CUP$parser$stack.elementAt(CUP$parser$top-1)), ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), RESULT);
            }
          return CUP$parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 0: // $START ::= spec EOF 
            {
              Object RESULT =null;
		int start_valleft = ((java_cup.runtime.Symbol)CUP$parser$stack.elementAt(CUP$parser$top-1)).left;
		int start_valright = ((java_cup.runtime.Symbol)CUP$parser$stack.elementAt(CUP$parser$top-1)).right;
		Node start_val = (Node)((java_cup.runtime.Symbol) CUP$parser$stack.elementAt(CUP$parser$top-1)).value;
		RESULT = start_val;
              CUP$parser$result = parser.getSymbolFactory().newSymbol("$START",0, ((java_cup.runtime.Symbol)CUP$parser$stack.elementAt(CUP$parser$top-1)), ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), RESULT);
            }
          /* ACCEPT */
          CUP$parser$parser.done_parsing();
          return CUP$parser$result;

          /* . . . . . .*/
          default:
            throw new Exception(
               "Invalid action number found in internal parse table");

        }
    }
}

