import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sun.org.apache.regexp.internal.RE;
import verificator.*;
import refal.*;

public class Verificator {

  private static Map<String, String> compares = new HashMap<>();

  public static void main(String argv[]) {
    for (int i = 0; i < argv.length; i+=2) {
      try {
        System.out.println("Verification ["+argv[i]+"]");
        VerificatorTreeBuilder verificatorTree = new VerificatorTreeBuilder(new VerificatorLexer(new FileReader(argv[i])));
        RefalTreeBuilder refalTree = new RefalTreeBuilder(new RefalFiveLexer(new FileReader(argv[i + 1])));
        refalTree.parse();
        verificatorTree.parse();
        //n.parse();
        System.out.println("No errors.");
        refalTree.getFunctions().forEach((RefalNode refalNode) -> {
          int j = 0;
          for (; j < verificatorTree.getFunctions().size(); j++) {
            if (refalNode.getName().equals(verificatorTree.getFunctions().get(j).getName())) {
              break;
            }
          }
          if (j == verificatorTree.getFunctions().size()) {
            System.out.println("Error");
            return;
          } else {
            System.out.println("It's Ok");
          }
          VerificatorNode verificatorNode = verificatorTree.getFunctions().get(j);
          for (int m = 0; m < refalNode.getChilds().size(); m++) {
            int k = m;
            for (; !refalNode.getChilds().get(k).getName().equals("VARS"); k++) {
            }
            if (!compareNodes(refalNode.getChilds().subList(m, k), verificatorNode.getChilds().get(0), verificatorTree)) {
              System.out.println("Error in" + refalNode.getChilds().subList(m, k));
            } else {
              System.out.println("OK");
            }
            m = k;
            if (checkResult(refalNode.getChilds().get(m).getChilds(), verificatorNode.getChilds().get(1), verificatorTree)) {
              System.out.println("OK");
            } else {
              System.out.println("Error in" + refalNode.getChilds().get(m).getChilds());
            }
          }
        });
      }
      catch (Exception e) {
        e.printStackTrace(System.out);
        System.exit(1);
      }
    }
  }

  private static boolean checkResult(List<RefalNode> refalNode, VerificatorNode verificatorNode, VerificatorTreeBuilder verificatorTree) {
    //System.out.println(refalNode);
    //System.out.println(verificatorNode);
    for (int i = 0; i < refalNode.size(); i++) {
      if (refalNode.get(i).getName().equals("PARENS")) {
        int j = 0;
        for (; j < verificatorTree.getTypeDef().size() && verificatorTree.getTypeDef().get(j).getName().equals(verificatorNode); j++) {
        }
        if(j == verificatorTree.getTypeDef().size()) {
          return false;
        }
        if(!compareResultTypes(refalNode.get(i), verificatorTree.getTypeDef().get(j), verificatorTree)) {
          return false;
        }
      } else if (refalNode.get(i).getName().equals("FUNC")) {
        return true;
      } else if (!compares.get(refalNode.get(i).getName()).equals(verificatorNode)) {
        return false;
      }
//      if (compares.containsKey(refalNode.get(i).getName()) && compares.get(refalNode.get(i).getName()).equals(verificatorTree.getTypeDef().get(j))) {
//      }
    }
    return true;
  }

  private static boolean compareResultTypes(RefalNode refalNode, VerificatorNode verificatorNode, VerificatorTreeBuilder verificatorTree) {
    //System.out.println(refalNode);
    //System.out.println(verificatorNode);
    for (int i = 0; i < verificatorNode.getChilds().size(); i++) {
      if (refalNode.getName().equals("PARENS") && verificatorNode.getChilds().get(i).getName().equals("PARENS")) {
        return compareResultNodes(refalNode.getChilds(), verificatorNode.getChilds().get(i), verificatorTree);
      } else if (refalNode.getName().equals(verificatorNode.getChilds().get(i).getName())) {
        return true;
      }
    }
    return false;
  }

  private static boolean compareResultNodes(List<RefalNode> refalNode, VerificatorNode verificatorNode, VerificatorTreeBuilder verificatorTree) {
    if (refalNode.size() != verificatorNode.getChilds().size()) {
      //System.out.println("1");
      return false;
    }
    for (int i = 0; i < refalNode.size(); i++) {
      //System.out.println(compares);
      if (refalNode.get(i).getName().equals("FUNC")) {
        return true;
      } else if (!compares.get(refalNode.get(i).getName()).equals(verificatorNode.getChilds().get(i).getName())) {
        int j = 0;
        for (; j < verificatorTree.getTypeDef().size() && verificatorTree.getTypeDef().get(j).getName().equals(verificatorNode.getChilds().get(i).getName()); j++) {
        }
        if(j == verificatorTree.getTypeDef().size()) {
          return false;
        }
        if(!compareResultTypes(refalNode.get(i), verificatorTree.getTypeDef().get(j), verificatorTree)) {
          return false;
        }
      }
    }
    return true;
  }

  private static boolean compareNodes(List<RefalNode> refalNode, VerificatorNode verificatorNode, VerificatorTreeBuilder verificatorTree) {
    //System.out.println(refalNode);
    //System.out.println(verificatorNode);
    if (refalNode.size() != verificatorNode.getChilds().size()) {
      return false;
    }
    for (int i = 0; i < refalNode.size(); i++) {
      if (!refalNode.get(i).getName().substring(0, 1).equals(verificatorNode.getChilds().get(i).getName().substring(0, 1)) && !refalNode.get(i).getName().contains("\'")) {
        int j = 0;
        for (; j < verificatorTree.getTypeDef().size() && verificatorTree.getTypeDef().get(j).getName().equals(verificatorNode.getChilds().get(i).getName()); j++) {
        }
        if(j == verificatorTree.getTypeDef().size()) {
          return false;
        }
        if(!compareTypes(refalNode.get(i), verificatorTree.getTypeDef().get(j - 1), verificatorTree)) {
          return false;
        }
      }
      compares.put(refalNode.get(i).getName(), verificatorNode.getChilds().get(i).getName());
    }
    return true;
  }

  private static boolean compareTypes(RefalNode refalNode, VerificatorNode verificatorNode, VerificatorTreeBuilder verificatorTree) {
    //System.out.println(refalNode);
    //System.out.println(verificatorNode);
    for (int i = 0; i < verificatorNode.getChilds().size(); i++) {
      if (refalNode.getName().equals("PARENS") && verificatorNode.getChilds().get(i).getName().equals("PARENS")) {
        return compareNodes(refalNode.getChilds(), verificatorNode.getChilds().get(i), verificatorTree);
      } else if (refalNode.getName().equals(verificatorNode.getChilds().get(i).getName())) {
        System.out.println(refalNode.getName());
        System.out.println(verificatorNode.getChilds().get(i).getName());
        return true;
      }
    }
    return false;
  }
}