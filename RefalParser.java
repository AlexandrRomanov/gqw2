import java.io.*;
import java_cup.runtime.Symbol;
import refal.*;

public class RefalParser {

  public static void main(String argv[]) {
      for (int i = 0; i < argv.length; i++) {
      try {
        System.out.println("Lexing ["+argv[i]+"]");
        RefalFiveLexer scanner = new RefalFiveLexer(new FileReader(argv[i]));
        RefalTreeBuilder p = new RefalTreeBuilder(new RefalFiveLexer(new FileReader(argv[i])));
        int j = 0;

        Symbol s;
        s = p.parse();
        System.out.println(p.getStart());
        System.out.println("No errors.");
      }
      catch (Exception e) {
        e.printStackTrace(System.out);
        System.exit(1);
      }
    }
  }
}
